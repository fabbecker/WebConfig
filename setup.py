from setuptools import setup, find_packages

setup(
    name="WebConfig",
    version="2022.09_pre",
    author="Fabian Becker",
    packages=find_packages(),
)
