import re
import string
from typing import List


def get_full_path(path: str = "/"):
    # Detect malicious paths
    if ".." in path:
        raise PermissionError("Detected a potential attack to look at files that are not meant to bee seen.")
    res = "/".join(__file__.split("/")[:-1]) + "/static" + path
    return res


def get_value_from_text(text):
    try:
        return int(text)
    except ValueError:
        try:
            return float(text)
        except ValueError:
            try:
                if re.match("True|False", text):
                    return eval(text)
                else:
                    raise ValueError(f"{text} can not be interpreted as boolean")
            except ValueError:
                return text


def get_template(path: str):
    return "/".join(__file__.split("/")[:-1]) + "/templates" + path


def get_text_as_url(text):
    letters = [x for x in text]
    for i, letter in enumerate(letters):
        if letter == " ":
            letters[i] = "+"
            continue
        if letter not in string.ascii_letters + string.digits:
            number = hex(ord(letter)).lstrip("0x")
            assert len(number.lstrip("%")) <= 2, f"Length of ascii is too long. {letter} -> {number} ({len(number)})"
            letters[i] = "%" + "0" * (2 - len(number)) + number
    return "".join(letters)


def get_url_as_text(text):
    text = text.replace("+", " ")
    for oc in re.findall(r"%[0-9a-z]{2}", text):
        text = text.replace(oc, bytearray.fromhex(oc[1:]).decode())
    return text


def get_http_header(data: str) -> dict:
    method, filename, scheme = data.split("\n")[0].strip().split(" ")
    res = {
        "method": method,
        "filename": filename,
        "scheme": scheme,
    }
    for line in data.split("\n")[1:]:
        res[line.split(": ")[0]] = ": ".join(line.split(": ")[1:])
    return res


def get_http_body(data: str) -> dict:
    res = {
        a.split("=")[0]: get_value_from_text(get_url_as_text(a.split("=")[1]))
        for a in data.split("&")
        if a
    }
    return res


def get_http_data(data: str) -> [dict, dict]:
    header, body = data.split("\r\n\r\n")
    return get_http_header(header), get_http_body(body)
