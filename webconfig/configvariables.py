from abc import ABC
from typing import Any


class ConfigVariable(ABC):
    type = None
    html_attributes = []

    def __init__(self, value: Any, id_: str, *args, **kwargs):
        self.value = value
        self.id = id_
        if "name" not in kwargs:
            self.name = id_
        else:
            self.name = kwargs["name"]

    def __str__(self):
        return self.value.__str__()

    def __eq__(self, other):
        return self.value == other

    def get_type(self):
        return type(self.value)

    def get_html(self):
        return "".join([
            "<input ",
            " ".join([
                f"{a}=\"{str(self.__getattribute__(a))}\"" for a in self.html_attributes
                if self.__getattribute__(a) is not None
            ]),
            ">"
        ])

    @staticmethod
    def get(value: Any, id_: str, *args, **kwargs):
        if type(value) == str:
            return ConfigStr(value=value, id_=id_)
        elif type(value) == float:
            return ConfigFloat(value=value, id_=id_, *args, **kwargs)
        elif type(value) == int:
            return ConfigInt(value=value, id_=id_, *args, **kwargs)


class ConfigStr(ConfigVariable):
    type = "text"
    html_attributes = ["type", "id", "name", "value"]

    def __init__(self, value: str, id_: str, *args, **kwargs):
        super(ConfigStr, self).__init__(value, id_, *args, **kwargs)

    def __str__(self):
        return self.value


class ConfigFloat(ConfigVariable):
    type = "number"
    html_attributes = ["type", "id", "name", "min", "max", "step", "value"]

    def __init__(self,
                 value: float, id_: str,
                 min_: [float, int] = None, max_: [float, int] = None, step: [float, int] = None,
                 *args, **kwargs):
        super(ConfigFloat, self).__init__(value, id_, *args, **kwargs)
        self.min = min_
        self.max = max_
        self.step = step

    def __float__(self):
        return self.value


class ConfigInt(ConfigFloat):
    type = "number"
    html_attributes = ["type", "id", "name", "min", "max", "step", "value"]

    def __init__(self, value: int, id_: str,
                 min_: int = None, max_: int = None):
        super(ConfigInt, self).__init__(value, id_, min_, max_, step=1)

    def __float__(self):
        return float(self.value)

    def __int__(self):
        return self.value
