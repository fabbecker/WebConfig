from abc import ABC
import logging
import socket
import threading
import time
from typing import List

from .requesthandler import RequestHandler
from .configvariables import ConfigVariable
from . import utils


class Configurable(ABC):
    configurable_attributes: List[str] = []
    configuration_update_function = None

    def __str__(self):
        return f"{self.__module__}.{self.__class__.__name__}"

    @property
    def url_name(self) -> str:
        return utils.get_text_as_url(str(self))

    def __bool__(self) -> bool:
        """
        Return true if there are configurable attributes available for this Configurable
        :return: bool
        """
        return len(self.configurable_attributes) > 0

    def set_configuration(self, key, value, update: bool = True):
        if key in dir(self):
            self.__setattr__(key, value)
        if update and self.configuration_update_function is not None:
            if callable(self.configuration_update_function):
                self.configuration_update_function()
            elif type(self.configuration_update_function) == str:
                self.__getattribute__(self.configuration_update_function)()

    def get_configuration(self, key: str = None):
        if key is not None:
            attr = self.__getattribute__(key)
            if not isinstance(attr, ConfigVariable):
                return ConfigVariable.get(value=attr, id_=key)
            else:
                return attr
        return {
            attr: self.get_configuration(attr) for attr in self.configurable_attributes
        }

    set_config = set_configuration
    get_config = get_configuration


class WebConfig(Configurable):
    def __init__(self, host: str = "0.0.0.0", port: int = 8080, start_server: bool = True, **context):
        self.request_handler = RequestHandler()
        self.context = None
        if context is not None:
            self.set_context(**context)

        self.s = socket.socket()
        self.host = host
        self.port = port

        self.thread = threading.Thread(target=self.manage_connections)
        self.thread.daemon = True
        self.running = False
        if start_server:
            self.start_server()

    def set_context(self, **context):
        self.context = context
        self.request_handler.set_context(self.context)

    def start_server(self):
        self.running = True
        self.thread.start()

    def stop_server(self):
        self.running = False
        self.thread.join()

    def manage_connections(self):
        t = 20
        for i in range(t):
            try:
                self.s.bind((self.host, self.port))
                print(f"Configuration is available at http://{self.host}:{self.port}/")
                break
            except OSError as e:
                if e.errno == 98:
                    logging.warning(f"Could not bind to {self.host} on port {self.port} ({i+1}/{t}): {e}")
                    time.sleep(5)
                else:
                    raise e
        self.s.listen()

        while self.running:
            # Establish connection with client.
            a = self.s.accept()
            # Answer to the request in a separate thread
            thread = threading.Thread(target=self.answer_connection, args=(a,))
            thread.start()
        self.s.detach()
        self.s.close()

    def answer_connection(self, a):
        c, (client_host, client_port) = a
        header = ""
        body = ""
        while True:
            new_data = c.recv(1)
            header += new_data.decode("utf-8")
            if not new_data:
                break
            if new_data == "":
                break
            if header.endswith("\r\n\r\n"):
                break
        if "Content-Length" in header:
            line = [l.lstrip("Content-Length: ") for l in header.split("\n") if l.startswith("Content-Length: ")][0]
            content_length = int(line)
        else:
            content_length = 0
        body += c.recv(content_length).decode("utf-8")
        return_value = self.request_handler(header + body).get_response()
        if type(return_value) == bytes:
            c.send(return_value)
        else:
            c.send(bytes(return_value, "utf-8"))
        c.close()
        return

    def __del__(self):
        self.stop_server()
