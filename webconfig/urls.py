from . import views


patterns = [
    ["/", views.home_view],
    ["/<namespace: str>/<object_name: str>/", views.info],
    ["/static", views.static_view],
]
