import re
from typing import List

from . import urls
from . import utils
from .responses import Response


class RequestHandler:
    @staticmethod
    def get_view(uri, patterns: List = urls.patterns) -> [callable, dict]:
        kwargs = {}
        for pat, view in patterns:
            if pat == uri:
                return view, kwargs
            segments_pat = [s for s in pat.split("/") if s]
            segments_uri = [s for s in uri.split("/") if s]
            if len(segments_pat) != len(segments_uri):
                continue
            for i, element in enumerate(segments_pat):
                if element != segments_uri[i]:
                    if element.startswith("<") and element.endswith(">"):
                        # Check for placeholders
                        key, value_raw = element[1:-1], segments_uri[i]
                        value = utils.get_url_as_text(value_raw)
                        if ": " in key:
                            key, type_ = key.split(": ")
                            try:
                                if type_ == "bool":
                                    if re.match("True|False", value):
                                        kwargs[key] = eval(value)
                                    else:
                                        raise ValueError(f"{value} can not be interpreted as boolean.")
                                else:
                                    kwargs[key] = eval(type_)(value)
                            except ValueError:
                                break
                        else:
                            kwargs[key] = utils.get_value_from_text(value)
                    else:
                        break
                if i + 1 == len(segments_uri):
                    return view, kwargs
        return None, {}

    def __init__(self, context: dict = None):
        self.context = None
        if context is not None:
            self.set_context(context)

    def set_context(self, context):
        self.context = context

    def get_object_from_context(self, namespace: str, object_name):
        context = self.context
        space = []
        for a in namespace.split("."):
            space = context[a]
        for obj in space:
            if str(obj) == object_name:
                return obj

    def __call__(self, data, *args, **kwargs) -> Response:
        uri = data.split("\n")[0].split(" ")[1]
        view, arguments = self.get_view(uri)
        if view is not None:
            res = view(self.context, data, **arguments)
            return res
        raise LookupError(f"No pattern found for {uri}")
