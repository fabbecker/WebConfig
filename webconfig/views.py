from . import responses
from . import configvariables
from . import utils


def home_view(context, data):
    res = responses.TemplateResponse()
    res.template = "/index.html"
    res.context = {"configurables": context}
    return res


def info(context, data, namespace: str, object_name: str):
    header, body = utils.get_http_data(data)
    if header['method'] == "POST":
        # Search for the plugin, which is to be modified
        for i, p in enumerate(context[namespace]):
            if str(p) == object_name:
                for a in body:
                    # apply every attribute in body to the plugin
                    if isinstance(p.__dict__[a], configvariables.ConfigVariable):
                        p.set_configuration(a, configvariables.ConfigVariable.get(body[a], a))
                    else:
                        p.set_configuration(a, body[a])
                        # p.__dict__[a] = body[a]
                if str(p) != object_name:
                    # redirect to new name if necessary
                    return responses.Redirect(f"/{namespace}/{p.url_name}")
                break
    try:
        obj = [p for p in context[namespace] if str(p) == object_name][0]
        return responses.TemplateResponse(
            template="/info.html",
            context={"object": obj, "namespace": namespace}
        )
    except IndexError:
        return responses.Redirect("/")  # return 404


def static_view(context, data, path: str):
    path = utils.get_full_path("/icon.svg")
    return responses.JPGResponse(
        image=path
    )
