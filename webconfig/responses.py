from jinja2 import Environment, FileSystemLoader

from . import utils


class Response:
    version = "HTTP/1.0"
    status_code = "200 OK"
    content_type: str = ""
    header_data: dict = {}
    body: str = ""

    def __init__(self, **kwargs):
        accepted = []
        parent = self.__class__
        while parent != object:
            for a in parent.__dict__.keys():
                accepted.append(a)
            parent = parent.__base__

        for k in kwargs:
            if k in accepted:
                self.__dict__[k] = kwargs[k]

    def __call__(self, *args, **kwargs):
        return self

    def get_header_data(self, key: str = None):
        data = {
            **({"Content-Type": self.content_type} if self.content_type else {}),
            **self.header_data,
        }
        if key is None:
            return data
        else:
            return data[key]

    def get_header(self) -> str:
        first_line = f"{self.version} {self.status_code}"
        last_lines = "\n".join([x + ": " + self.get_header_data(x) for x in self.get_header_data().keys()])
        return first_line + "\n" + last_lines + "\n\n"

    def get_body(self) -> str:
        return self.body or ""

    def get_response(self):
        return self.get_header() + self.get_body()


class HTMLResponse(Response):
    content_type = "text/html"


class TemplateResponse(HTMLResponse):
    template: str = None
    context: dict = None

    def get_body(self) -> str:
        loader = FileSystemLoader(utils.get_template("/"))
        env = Environment(loader=loader)
        template = env.get_template(self.template)
        return template.render(**(self.context or {}))


class FileResponse(Response):
    def __init__(self, path: str = None, *args, **kwargs):
        super(FileResponse, self).__init__()
        self.data = None
        if path is not None:
            self.set_path(path)

    def set_path(self, path: str):
        self.data = open(path, "rb").read()

    def get_response(self):
        response = b'\n'.join([
            bytes(f"{self.version} {self.status_code}", "utf-8"),
            b"Connection: close",
            # b"Content-Type: " + bytes(self.content_type, "utf-8"),
            bytes("Content-Length: %s" % len(self.data), "utf-8"),
            b'', self.data
        ])
        return response


class ImageResponse(FileResponse):
    def __init__(self, image, *args, **kwargs):
        super(ImageResponse, self).__init__(path=None, *args, **kwargs)
        if type(image) == str:
            self.set_path(path=image)


class JPGResponse(ImageResponse):
    content_type = "image/jpg"


class Redirect(TemplateResponse):
    status_code = "302 Redirect"
    template = "redirect.html"

    def __init__(self, location: str = None, *args, **kwargs):
        super(Redirect, self).__init__(*args, **kwargs)
        if location is not None:
            self.header_data["Location"] = location

    @property
    def context(self):
        return {
            "location": self.header_data["Location"]
        }

