import string
import unittest

from webconfig import Configurable, WebConfig, configvariables, responses, requesthandler, utils, views


class Configuration(unittest.TestCase):
    def test_find_and_configure_object(self):
        class TestObject(Configurable):
            def __init__(self):
                self.a = 1

        t_obj = TestObject()

        cfg = WebConfig(my_test_objects=[t_obj])

        self.assertEqual(t_obj, cfg.request_handler.get_object_from_context("my_test_objects", str(t_obj)))

        cfg.request_handler.get_object_from_context("my_test_objects", str(t_obj)).set_configuration("a", 2)
        self.assertEqual(2, t_obj.a)

    def test_config_str(self):
        cs = configvariables.ConfigVariable.get(value="abc", id_="cs")
        expected = "<input type=\"text\" id=\"cs\" name=\"cs\" value=\"abc\">"
        self.assertEqual(
            expected,
            cs.get_html(),
            "ConfigStr is not represented as html correctly."
        )

    def test_config_str_with_special_characters(self):
        class DummyObject(Configurable):
            configurable_attributes = ["cv"]

            def __init__(self):
                self.cv = configvariables.ConfigVariable.get(value="initial value", id_="cs")
        my_object = DummyObject()

        test_string = string.printable

        views.info(
            context={
                'objects': [
                    my_object,
                ]
            },
            data="POST / HTTP\r\n\r\ncv=" + utils.get_text_as_url(test_string),
            namespace='objects',
            object_name=str(my_object),
        )
        self.assertEqual(
            test_string,
            my_object.cv.value,
            "Texts with spaces are not set."
        )

    def test_config_float(self):
        cf = configvariables.ConfigVariable.get(value=1.23, id_="cf")
        expected = "<input type=\"number\" id=\"cf\" name=\"cf\" value=\"1.23\">"
        self.assertEqual(
            expected,
            cf.get_html(),
            "ConfigFloat is not represented as html correctly."
        )

    def test_config_int(self):
        ci = configvariables.ConfigVariable.get(value=1, id_="ci")
        expected = "<input type=\"number\" id=\"ci\" name=\"ci\" step=\"1\" value=\"1\">"
        self.assertEqual(
            expected,
            ci.get_html(),
            "ConfigFloat is not represented as html correctly."
        )

    def test_config_from_builtin_datatypes(self):
        class DummyObject(Configurable):
            configurable_attributes = ["cv"]

            def __init__(self):
                self.cv = 1
        my_object = DummyObject()
        expected = "<input type=\"number\" id=\"cv\" name=\"cv\" step=\"1\" value=\"1\">"
        self.assertEqual(
            expected,
            my_object.get_config("cv").get_html(),
            "Builtin data types are not converted."
        )

    def test_convert_config_variables_to_builtin_datatypes(self):
        s = configvariables.ConfigVariable.get("str", "s")
        self.assertEqual("str", str(s), "ConfigStr is not converted to string")
        f = configvariables.ConfigVariable.get(1.0, "f")
        self.assertEqual(1.0, float(f), "ConfigFloat is not converted to string")
        i = configvariables.ConfigVariable.get(1, "i")
        self.assertEqual(1, int(i), "ConfigInt is not converted to string")


class RequestHandlerAndViewsTest(unittest.TestCase):
    def test_pattern_detection(self):
        rh = requesthandler.RequestHandler()
        patterns = [
            ["/simple_pattern", 0],
            ["/users/<username: str>", 1],
            ["/<a>/<b>/<c>", 2],
            ["/datatypes/<int: int>/", 3],
            ["/datatypes/<float: float>/", 4],
            ["/datatypes/<boolean: bool>/", 5],
            ["/", 6],
        ]
        self.assertEqual(
            (0, {}),
            rh.get_view(uri="/simple_pattern", patterns=patterns),
            "Simple patterns are not detected."
        )
        self.assertEqual(
            (1, {"username": "testuser"}),
            rh.get_view(uri="/users/testuser", patterns=patterns),
            "Patterns with arguments are not detected."
        )
        self.assertNotEqual(
            (1, {"username": "testuser"}),
            rh.get_view(uri="/users/1", patterns=patterns),
            "Wrong datatypes are still used."
        )
        self.assertEqual(
            (2, {"a": 1, "b": 2, "c": 3}),
            rh.get_view(uri="/1/2/3", patterns=patterns),
            "Patterns with multiple arguments are not detected."
        )
        self.assertEqual(
            (2, {"a": 1.0, "b": 2.1, "c": -34.2}),
            rh.get_view(uri="/1.0/2.1/-34.2", patterns=patterns),
            "Patterns with number arguments are not detected."
        )
        self.assertEqual(
            (5, {"boolean": True}),
            rh.get_view(uri="/datatypes/True", patterns=patterns),
            "Boolean True is not detected."
        )
        self.assertEqual(
            (5, {"boolean": False}),
            rh.get_view(uri="/datatypes/False", patterns=patterns),
            "Boolean False is not detected."
        )
        self.assertEqual(
            (None, {}),
            rh.get_view(uri="/datatypes/raise SystemError(\"Malicious scripts are not detected\")", patterns=patterns),
            "Malicious scripts are detected as booleans."
        )
        self.assertEqual(
            (6, {}),
            rh.get_view(uri="/", patterns=patterns),
            "Pattern / not detected."
        )

    def test_config_variable_representation(self):
        class DummyObject(Configurable):
            configurable_attributes = ["cv"]

            def __init__(self):
                self.cv = 1
        my_object = DummyObject()
        res = views.info(
            context={
                'objects': [
                    my_object,
                ]
            },
            data="GET / HTTP\r\n\r\n",
            namespace="objects",
            object_name=str(my_object),
        )
        self.assertIn(
            my_object.get_config("cv").get_html(),
            res.get_response(),
            "ConfigVariable not represented in response."
        )

    def test_change_values(self):
        class DummyObject(Configurable):
            configurable_attributes = ["cv"]

            def __init__(self):
                self.v = 1
                self.cv = configvariables.ConfigVariable.get("", "cv")
        my_object = DummyObject()
        views.info(
            context={
                'objects': [
                    my_object,
                ]
            },
            data="POST / HTTP\r\n\r\nv=0&cv=abc",
            namespace='objects',
            object_name=str(my_object),
        )
        self.assertEqual(
            0,
            my_object.v,
            "Changes made via POST requests are not persistent."
        )
        self.assertEqual(
            configvariables.ConfigVariable.get("abc", "cv"),
            my_object.cv,
            "Changes made via POST requests are not persistent."
        )


class UtilsTest(unittest.TestCase):
    def test_path_detection(self):
        self.assertRaises(PermissionError, utils.get_full_path, "/..")

    def test_url_text_conversion(self):
        orig = "diyarvu.plugins.background.Color(0, 0, 0)"
        self.assertEqual(
            orig,
            utils.get_url_as_text(utils.get_text_as_url(orig)),
            "URLs are not converted correctly."
        )


class Responses(unittest.TestCase):
    def test_attribute_overwrite(self):
        r = responses.Response(body="ok")
        self.assertEqual("ok", r.body, "Attributes are not set on __init__")

    def test_redirects(self):
        loc = "/"
        r = responses.Redirect(header_data={"Location": loc})
        self.assertEqual(loc, r.header_data["Location"])


if __name__ == '__main__':
    unittest.main()
