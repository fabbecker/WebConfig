# WebConfig
… is a python module that can be used on embedded devices to host a website to change settings by using a web browser on any computer.

## Where is all the development process?
This project originated from [another project](https://codeberg.org/diyarvu/diyarvu/src/branch/config-over-network/diyarvu/plugins/ui/webui) and is meant to be a plugin. But eventually it became so big and versatile it made sense to exclude it from the original project and make it available for other projects as well.
